import { got } from "./data.js";

function countAllPeople(got) {
  var result;
  Object.keys(got).map((houses) => {
    result = got[houses].reduce((accumulator, currentHouse) => {
      accumulator += currentHouse.people.length;
      return accumulator;
    }, 0);
  });
  return result;
}

function peopleByHouses(got) {
  let result = {};
  Object.keys(got).map((houses) => {
    result = got[houses].reduce((accumulator, currentHouse) => {
      accumulator[currentHouse.name] = currentHouse.people.length;
      return accumulator;
    }, {});
  });
  return result;
}

function everyone(got) {
  let result = [];
  Object.keys(got).map((houses) => {
    got[houses].filter((currentHouse) => {
      currentHouse.people.filter((currentName) => {
        result.push(currentName.name);
      });
    });
  });
  return result;
}

function nameWithS(got) {
  let result = [];
  Object.keys(got).map((houses) => {
    got[houses].map((currentHouse) => {
      currentHouse.people.filter((currentName) => {
        if (currentName.name.includes("s") || currentName.name.includes("S")) {
          result.push(currentName.name);
        }
      });
    });
  });
  return result;
}

function nameWithA(got) {
  let result = [];
  Object.keys(got).map((houses) => {
    got[houses].map((currentHouse) => {
      currentHouse.people.filter((currentName) => {
        if (currentName.name.includes("a") || currentName.name.includes("A")) {
          result.push(currentName.name);
        }
      });
    });
  });
  return result.length;
}

function surnameWithS(got) {
  let result = [];
  Object.keys(got).map((houses) => {
    got[houses].map((currentHouse) => {
      currentHouse.people.filter((currentName) => {
        const nameArray = currentName.name.split(" ");
        if (nameArray[1].slice(0, 1) === "S") {
          result.push(nameArray[0] + " " + nameArray[1]);
        }
      });
    });
  });
  return result;
}

function surnameWithA(got) {
  let result = [];
  Object.keys(got).map((houses) => {
    got[houses].map((currentHouse) => {
      currentHouse.people.filter((currentName) => {
        const nameArray = currentName.name.split(" ");
        if (nameArray[1].slice(0, 1) === "A") {
          result.push(nameArray[0] + " " + nameArray[1]);
        }
      });
    });
  });
  return result;
}

function peopleNameOfAllHouses(got) {
  let result = {};
  Object.keys(got).map((houses) => {
    got[houses].map((currentHouse) => {
      result[currentHouse.name] = [];
      currentHouse.people.map((namePeople) => {
        result[currentHouse.name].push(namePeople.name);
      });
    });
  });
  return result;
}
